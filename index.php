<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>s02 Activity</title>
</head>
<body>
	<h1>Divisibles of Five</h1>
	<p><?php echo divBy5() ?></p>
	<br>
	<h1>Array Manipulation</h1>
	<p><?php array_push($students, 'John Smith') ?></p>
	<pre><?php print_r($students) ?></pre>
	<pre><?php echo count($students) ?></pre>
	<p><?php array_push($students, 'Jane Smith') ?></p>
	<pre><?php print_r($students) ?></pre>
	<pre><?php echo count($students) ?></pre>
	<p><?php array_shift($students) ?></p>
	<pre><?php print_r($students) ?></pre>
	<pre><?php echo count($students) ?></pre>


</body>
</html>
